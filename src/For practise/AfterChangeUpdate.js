import React, { useState, useRef } from "react";
import { Button, Table } from "react-bootstrap";
import { db } from "../firebase";

export default function TodoListItem({
  todo,
  notCompleted,
  id,
  todoTnputF,
  setTodoTnputF,
  setTodoInput,
  index,
  inputTextField,
  todos,
}) {
  const inputRef = useRef();
  // const [forEdit, setForEdit] = useState("");

  function deleteTodo() {
    db.collection("todos").doc(id).delete();
  }

  function editTodo() {
    inputTextField.current.value = todo;

    var y = todos.find((tod) => {
      if (tod.id === id) {
        db.collection("todos").doc(id).update({ todo: todoTnputF });
      }
    });
    // setTodoTnputF("");
    setTodoInput("");
  }

  // setForEdit("");

  // console.log(inputTextField.current.value);

  // console.log(todoTnputF);

  // var x = todos.find((tod) => {
  //   return tod.id === id;
  // });
  // console.log(x);
  // if (inputTextField.current.value !== todoTnputF) {
  // } else {
  //   db.collection("todos").doc(id).update({ todo: todoTnputF });
  // }
  // setTodoInput("");

  // if (todoTnputF == inputTextField.current.value) {
  //   console.log("Different");
  // } else {
  //   console.log("same");
  // }

  // setTodoTnputF(inputTextField.current.value);
  // console.log(todoTnputF);
  // setTodoInput(todoTnputF);
  // db.collection("todos").doc(id).update({ todo: todoTnputF });
  // console.log(todoTnputF);
  // setTodoInput("");
  //}
  // function conEditTodo() {
  //   db.collection("todos").doc(id).update({ todo: todoTnputF });
  //   // console.log(todoTnputF);
  //   setTodoInput("");
  // }

  return (
    <div>
      {/* <h3 className="d-flex align-items-left ">{todo}</h3>
      <Button className="d-flex align-item-right " onClick={deleteTodo}>
        Delete
      </Button> */}
      <Table class="table  ">
        <thead></thead>
        <tbody>
          <tr>
            <th scope="row"></th>
            <td>{index + 1}</td>

            <td>{todo}</td>
            <td>
              <Button
                className="d-flex align-button-right justify-content-right "
                onClick={deleteTodo}
              >
                Delete
              </Button>
            </td>
            <td>
              <Button
                className="d-flex align-item-right "
                ref={inputRef}
                onClick={editTodo}
              >
                Edit?
              </Button>
            </td>
            {/* <td>
              <Button
                ref={inputRef}
                style={{ maxWidth: "100px" }}
                onClick={conEditTodo}
              >
                Done to Edit
              </Button>
            </td> */}
          </tr>
        </tbody>
      </Table>

      {/* <ListGroup>
        <ListGroupItem>
          <h3 className="d-flex align-items-left ">{todo}</h3>
          <Button className="d-flex align-item-right " onClick={deleteTodo}>
            Delete
          </Button>
        </ListGroupItem>
      </ListGroup> */}
    </div>
  );
}
