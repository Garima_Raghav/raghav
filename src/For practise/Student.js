import React, { useState, useEffect } from "react";
import { Card, Button, Alert, Table } from "react-bootstrap";
import { useAuth } from "../context/AuthContext";
import { Link, useHistory } from "react-router-dom";
import { db } from "../firebase";
import firebase from "firebase";
import TodoListItemStudent from "./ToDoS";

export default function Dashboard() {
  const [todos, setTodos] = useState([]);
  const [error, setError] = useState("");
  const { currentUser, logout } = useAuth();
  const history = useHistory();

  useEffect(() => {
    getTodos();
  }, []);

  function getTodos() {
    db.collection("todos").onSnapshot(function (querySnapshot) {
      setTodos(
        querySnapshot.docs.map((doc) => ({
          id: doc.id,
          todo: doc.data().todo,
          notCompleted: doc.data().notCompleted,
        }))
      );
    });
  }

  async function handleLogout() {
    setError("");
    try {
      await logout();
      history.push("/login");
    } catch {
      setError("Failed to log out");
    }
  }

  return (
    <>
      <div className="row">
        <div className="block col-1 w-100" style={{ minWidth: "400px" }}>
          <Card>
            <Card.Body>
              <h2 className="text-center mb-4">Profile</h2>
              {error && <Alert variant="danger">{error}</Alert>}
              <strong>Email:</strong>
              {currentUser.email}

              <Link to="/update-profile" className="btn btn-primary w-100 mt-3">
                Update Profile
              </Link>
              <div className="w-100 text-center mt-2">
                <Button variant="link" onClick={handleLogout}>
                  Log Out
                </Button>
              </div>
            </Card.Body>
          </Card>
        </div>

        <div className="block col-2 " style={{ minWidth: "700px" }}>
          <Table>
            <thead>
              <tr>
                <th>S.No.</th>
                <th>Title</th>
                <th>Work Status</th>
                <th> Mark</th>
              </tr>
            </thead>
          </Table>
          <div className="w-300" style={{ maxWidth: "700px" }}>
            {todos.map((todo, i) => (
              <TodoListItemStudent
                todo={todo.todo}
                notCompleted={todo.notCompleted}
                id={todo.id}
                index={i}
              />
            ))}
          </div>
        </div>
      </div>
    </>
  );
}

import React from "react";
import { ListGroup, ListGroupItem, Button } from "react-bootstrap";
import { db } from "../firebase";

export default function TodoListItemStudent({ todo, notCompleted, id, index }) {
  function toDone() {
    db.collection("todos").doc(id).update({
      notCompleted: !notCompleted,
    });
    console.log("For students only");
  }

  return (
    <div>
      <table class="table  ">
        <thead></thead>
        <tbody>
          <tr>
            <th scope="row"></th>
            <td>{index + 1}</td>

            <td>{todo}</td>
            <td>{notCompleted ? "Not Completed" : "Completed"}</td>
            <td>
              <Button onClick={toDone}>
                {notCompleted ? "Done" : "Not Done"}
              </Button>
            </td>
          </tr>
        </tbody>
      </table>
      {/* <ListGroup>
        <ListGroupItem>
          <h3 className="d-flex align-items-left ">{todo}</h3>

          <h6>{notCompleted ? "Not Completed" : "Completed"}</h6>
          <Button onClick={toDone}>{notCompleted ? "Done" : "Not Done"}</Button>
        </ListGroupItem>
      </ListGroup> */}
    </div>
  );
}
