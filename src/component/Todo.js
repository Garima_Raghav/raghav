import React, { useState, useEffect } from "react";
import { ListGroup, ListGroupItem, Button, Table } from "react-bootstrap";
import { db } from "../firebase";

export default function TodoListItem({
  todo,
  notCompleted,
  id,
  todoTnputF,
  setTodoInput,
  index,
}) {
  var [count, setCount] = useState(0);
  useEffect(() => {
    setCount(count + 1);
  }, []);
  function deleteTodo() {
    db.collection("todos").doc(id).delete();
  }

  function editTodo() {
    db.collection("todos").doc(id).update({ todo: todoTnputF });
    setTodoInput("");
  }

  return (
    <div>
      {/* <h3 className="d-flex align-items-left ">{todo}</h3>
      <Button className="d-flex align-item-right " onClick={deleteTodo}>
        Delete
      </Button> */}
      <table class="table  ">
        <thead></thead>
        <tbody>
          <tr>
            <th scope="row"></th>
            <td>{index + 1}</td>

            <td>{todo}</td>
            <td>
              <Button
                className="d-flex align-button-right justify-content-right "
                onClick={deleteTodo}
              >
                Delete
              </Button>
            </td>
            <td>
              <Button className="d-flex align-item-right " onClick={editTodo}>
                Edit
              </Button>
            </td>
          </tr>
        </tbody>
      </table>

      {/* <ListGroup>
        <ListGroupItem>
          <h3 className="d-flex align-items-left ">{todo}</h3>
          <Button className="d-flex align-item-right " onClick={deleteTodo}>
            Delete
          </Button>
        </ListGroupItem>
      </ListGroup> */}
    </div>
  );
}
